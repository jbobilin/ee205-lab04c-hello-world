///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World Cpp
//
// File: hello1.cpp
// Usage: hello1
//
// Result:
// simple "hello world" program in c++ using std namespace
//
//
// @author Jonah Bobilin <jbobilin@hawaii.edu>
// @date   11 February 2021
///////////////////////////////////////////////////////////////////////////////

using namespace std;

#include <iostream>

int main(void) {
   cout << "Hello World!" << endl;
   return 0;
}




