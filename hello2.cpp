///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World Cpp
//
// File: hello2.cpp
// Usage: hello2
//
// Result:
// simple "hello world" program in c++ without std namespace
//
//
// @author Jonah Bobilin <jbobilin@hawaii.edu>
// @date   11 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main(void) {
   std::cout << "Hello World!" << std::endl;
   return 0;
}




